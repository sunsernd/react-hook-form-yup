import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const App = () => {
  const schema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required().min(8).max(32),
  });

  const { register, handleSubmit, formState: { errors }, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = (data) => {
    console.log({ data });
    reset();
    //navigate to next page
  };

  console.log('error ', errors);

  return (
    <form onSubmit={handleSubmit(onSubmitHandler)}>
      <h2>React Hook Form with Yup</h2>
      <br />

      <input 
        {...register("email")}
        placeholder="email" 
      />
       <div className="errorMsg">{errors.email?.message}</div>
      <br />

      <input 
        {...register("password")}
        placeholder="password"
        type="password"
      />
      <div className="errorMsg">{errors.password?.message}</div>
      <br />
      <br />

      <button type="submit">Sign in</button>
    </form>
  );
};

export default App;